'''
Created on 7-jun.-2012

@author: Menno
'''
import pygame
from pygame.locals import *
import os, sys, shutil, logging, time
import EventClassFile
from NodeClassFile import Node, NodeGroup
from NodeGroupsClassFile import NodeGrouping

#Main class
class Browser(EventClassFile.Event):
    def __init__(self, xSize = 1280, ySize = 720):
        self._running = False
        self._screen = None
        self.size = self.width, self.height = xSize, ySize
        self.on_init()
        self.zoom = 1
        self.font = pygame.font.SysFont('Times New Roman', 14)
        self.clock = pygame.time.Clock()
        self.clicked = None
        self.mouseLDown = False
        self.mouseMDown = False
        self.mousePos = (0, 0)
        #Actually a method which has to store some of it's values
        self._double_clicked = DoubleClick(self)
        print self.size 
                
        #Tests#########
        self.nodeGroups = NodeGrouping()
        fileList = sorted(map(lambda x: os.path.join("C:\\\\", x), os.listdir("C:\\\\")))
        print fileList
        self.nodeGroups.add_groups(NodeGroup( "root", self.size, self.zoom, 1, self.font))
        #self.nodeGroups.add_groups(NodeGroup(fileList, self.size, 1, 2, self.font,
        #                                      parent = list(self.nodeGroups.get_groups_by_sprites( lambda sprites: "C:\\\\" in map(lambda sprite: sprite.path, sprites) ) )[0].sprites()[0]))
        #self.group = NodeGroup(fileList, self.size, 1, 2, self.font, parent = self.c.sprites()[0])
        ###############
        
    def _set_size(self, size):
        self.size = size
        self.width = size[0]
        self.height = size[1]
                
    def on_init(self):
        pygame.init()
        self._screen = pygame.display.set_mode(self.size, pygame.RESIZABLE|pygame.HWSURFACE)
        pygame.display.set_caption('Gource-Like File Browser')
        self._running = True
        return True
            
    def on_loop(self):
        pass
    
    def on_render(self):
        self._screen.fill((0, 0, 0), pygame.Rect(0,0,self.width, self.height))
        self.nodeGroups.draw(self._screen)
        pygame.display.flip()
    
    def on_cleanup(self):
        pygame.quit()
    
    def on_execute(self):
        'The start of the program'
        #game loop
        while( self._running ):
            self.clock.tick()
            #Event loop
            for event in pygame.event.get():
                self.on_event(event)
            #Logic loop
            self.on_loop()
            #render loop
            self.on_render()
            #print self.clock.get_fps()
        self.on_cleanup()
        
    ######################Extended events from template class EventClassFile.Event###################################
    def on_exit(self):
        self._running = False
        
    def on_resize(self, event):
        print 'resized'
        self._set_size(event.size)
        
    def on_lbutton_down(self, event):
        self.mouseLDown = True
        print "Click"
        collisions = self.nodeGroups.mouse_collision(event.pos)
        self.clicked = collisions
        if self._double_clicked(self.clicked):
            print "double clicked!"
            if self.clicked.isDir:
                self.clicked.expand(self.nodeGroups)
            else:
                self.clicked.run()
            
    def on_lbutton_up(self, event):
        self.clicked = None
        self.mouseLDown = False
        
    def on_mouse_wheel_up(self, event):
        print "up"
        self.zoom += 0.25
        self.nodeGroups.update_zoom(self.zoom)
        
    def on_mouse_wheel_down(self, event):
        print"down"
        self.zoom -= 0.25
        self.nodeGroups.update_zoom(self.zoom)
        
    def on_mbutton_down(self, event):
        self.mouseMDown = True
    def on_mbutton_up(self, event):
        self.mouseMDown = False
        
    def on_mouse_move(self, event):
        newMousePos = pygame.mouse.get_pos()
        if self.clicked:
            offset = (event.pos[0] - self.clicked.rect.center[0], event.pos[1] - self.clicked.rect.center[1])
            self.clicked.move_offset(offset)
        elif self.mouseMDown:
            offset = (newMousePos[0] - self.mousePos[0], newMousePos[1] - self.mousePos[1])
            list(list(self.nodeGroups())[0].sprites())[0].move_offset(offset)
        self.mousePos = newMousePos

class DoubleClick():
    def __init__(self, browser):
        self.node = tuple()
        self.browser = browser
    def __call__(self, node):
        newTime = time.time()
        if node:
            if self.node:
                timeDif = newTime - self.node[1]
                print timeDif
                if node == self.browser.clicked and timeDif < 0.3:
                    return True
        self.node = (node, newTime)
        return False