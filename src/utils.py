'''
Created on 13-jun.-2012

@author: Menno
'''
from math import sqrt
def distance(t1, t2):
    return sqrt((t1[0]-t2[0])**2 + (t1[1] - t2[1])**2)

def a_to_b(a, b, multiplier=1):
    return ( (b[0] - a[0]) * multiplier, (b[1] - a[1]) * multiplier)