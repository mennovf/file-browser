'''
Created on 11-jun.-2012

@author: Menno
'''
import pygame, logging
from pygame.locals import *

class NodeGrouping:
    def __init__(self, *groups):
        self.lineColor = (255, 255, 255)#White
        self.groups = []
        self.add_groups(*groups)
            
    def add_groups(self, *groups):
        for group in groups:
            self.groups.append(group)
            
    def __call__(self):
        return self.get_groups()
    
    def get_groups(self):
        for group in self.groups:
            yield group
            
    #Func should take a list of sprite as input and return True or false
    def get_groups_by_sprites(self, func):
        for group in self.groups:
            if func( group.sprites() ):
                yield group
    
    def get_groups_by_group(self, func):
        for group in self.groups:
            if func(group):
                yield group
            
    def update(self):
        for group in self.groups:
            group.update()
            
    def draw(self, destination):
        self._drawlines(destination)
        for group in self.groups:
            group.draw(destination)
            
    def _drawlines(self, destination):
        for group in self.groups:
            for sprite in group:
                if not group.parent is None:
                    pygame.draw.aaline(destination, self.lineColor, group.parent.rect.center, sprite.rect.center)
                    
    def mouse_collision(self, mousePos):
        sprites = list()
        mouseSprite = pygame.sprite.Sprite()
        mouseSprite.rect = pygame.Rect(mousePos, (1,1))
        for group in reversed(self.groups):
            sprites = pygame.sprite.spritecollide(mouseSprite, group, False)
            if sprites:
                return sprites[0]
            
    def remove(self, *groups):
        for group in groups:
            self.groups.remove(group)
            
    def update_zoom(self, zoom):
        for group in sorted(self.groups, key=lambda x: x.depth):
            group.update_zoom(zoom)