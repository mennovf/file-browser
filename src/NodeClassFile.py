'''
Created on 7-jun.-2012

@author: Menno
'''
import pygame
import os, logging
import math
import utils

class Node(pygame.sprite.Sprite):
    '''
    The class for the nodes, representing the paths.
    Contains: Image(a Circle), the position, the absolute path it represents,  
    '''
    def __init__(self, parentGroup, path, position, fonter):
        '''
        Constructor
        '''
        pygame.sprite.Sprite.__init__(self)
        #Figure out the size of the sprite
        self.image = parentGroup.image
        self.isDir = True
        if os.path.isfile(path):
            print '%s is a file' % path
            self.image = parentGroup.fileImage
            self.isDir = False
        self.parentGroup = parentGroup
        self.isExpanded = False
        self.prevZoom = parentGroup.zoom
        self.rect = self.image.get_rect().copy()
        self.children = []
        self.realPath = path
        print self.realPath
        self.path = path.split('\\')
        print str(self.path),  path 
        self.path = self.path[len(self.path)-1]
        self.path = "C:\\\\" if not self.path else self.path
        self.pathImage = fonter.render(self.path, True, (255,255,255) )
        print self.rect.center
        self.rect.center = position
        self.x = self.rect.center[0]
        self.y = self.rect.center[1]
        
    def move_offset(self, offset):
        for child in self.children:
            child.move_offset(offset)
        self.rect.center = (self.rect.center[0] + offset[0], self.rect.center[1] + offset[1])
        self.x = self.rect.center[0]
        self.y = self.rect.center[1]
        
    def update_image(self):
        parentGroup = self.groups()[0]
        self.image = parentGroup.image
        if not self.isDir:
            self.image = parentGroup.fileImage
        print self.image
        newRect = self.image.get_rect().copy()
        newRect.center = self.rect.center
        if parentGroup.parent:
            #Rethink this
            fromOrigin = (self.x - parentGroup.parent.rect.center[0], self.y - parentGroup.parent.rect.center[1])
            distance = math.sqrt(fromOrigin[0]**2 + fromOrigin[1]**2)
            changedZoom = parentGroup.zoom - self.prevZoom
            newDist = max( 0.1 , distance + (distance/changedZoom) )
            fromOrigin = (fromOrigin[0]/distance*newDist, fromOrigin[1]/distance*newDist)
            print fromOrigin
            self.prevZoom = parentGroup.zoom
            newRect.center = (fromOrigin[0] + parentGroup.parent.rect.center[0], fromOrigin[1] + parentGroup.parent.rect.center[1])
        self.rect = newRect
        print self.rect
        
    def expand(self, nodeList):
        if not self.isExpanded:
            #paths = filter(lambda x: os.access(x, os.F_OK | os.R_OK), os.listdir(self.realPath))
            paths =  []
            try:
                paths = map(lambda x: os.path.join(self.realPath, x), os.listdir(self.realPath))
            except WindowsError:
                    pass
        
            print paths
            p = self.groups()[0]
            print "paths: %s, parentG: %s" % (paths, p)
            if paths:
                nodeList.add_groups( NodeGroup(paths, p.canvasSize, p.zoom,  p.depth + 1, p.fonter, self) )
                self.isExpanded = True
        else:
            self._deexpand(nodeList)
            
    def run(self):
        os.startfile(self.realPath)
        
    def _deexpand(self, nodeList):
        if self.children:
            for group in self.children[0].groups():
                for sprite in group.sprites():
                    sprite._deexpand(nodeList)
                group.empty()
                nodeList.remove(group)
            self.isExpanded = False
        
class NodeGroup(pygame.sprite.Group):
    
    def __init__(self, paths, canvasSize, zoom, depth, fonter,  parent = None):
        pygame.sprite.Group.__init__(self)
        self.root = False
        self.image = None
        self.depth = 1
        self.dirColor = (40, 240, 10) #Green ish color
        self.fileColor = (240 ,240, 0) #Yellow ish color
        self.size = 0
        self.fonter =  fonter
        self.depth = depth
        self.zoom = zoom
        self.canvasSize = canvasSize
        self.distanceFromParent = 1
        self.parent = parent
        if not parent:
            self._add_root(canvasSize, zoom, depth, fonter, update = True)
            return
        self._add_nodes( paths, canvasSize, zoom, depth, fonter, update=True )
                
    def _gen_image(self):
        self.size = max( 1, int( ( min(list(self.canvasSize)) / (10*(self.depth)) ) * self.zoom ) )
        self.distanceFromParent = self.size * 5
        self.image = pygame.Surface( (self.size, self.size) )
        black = (0, 0, 0)
        self.image.set_colorkey(black)
        self.image.fill(black)
        self.fileImage = self.image.copy()
        halfSize = self.size/2
        pygame.draw.circle(self.image, self.dirColor, (halfSize, halfSize), halfSize )
        pygame.draw.circle(self.fileImage, self.fileColor, (halfSize, halfSize), halfSize)
    
    def _add_root(self, canvasSize, zoom, depth, fontObject, update=False):
        self.root = True
        if update:
            self.dirColor = (200,200,200)#Root is white circle
            self._gen_image()
            
        x = canvasSize[0]/2
        y = canvasSize[1]/2
        print "%s, %s" % (x, y)
        self.add( Node( self, 'C:\\\\', (x, y), fontObject ) )
        
    def _add_nodes(self, paths, canvasSize, zoom, depth, fontObject, update=False):
        'Creates a shared image for all the nodes, creating this image uses canvasSize, zoom and depth to define the size of the circle'
        if update:
            #Create the shared surface used by all the siblings
            self._gen_image()
        #Add a Node(Sprite) for every path in paths
        if len(paths):
            offsets = self._calculate_radial_positions(len(paths), self.distanceFromParent)
            parentPos = self.parent.rect.center
            for i, path in enumerate(paths):
                nodePos = (parentPos[0] + offsets[i][0], parentPos[1] + offsets[i][1])
                newNode = Node(self, path,  nodePos, fontObject)
                self.parent.children.append(newNode)
                self.add( newNode )
                print 'Added Node at depth: %s, size: %s, path: %s' % (depth, self.size, path)
    
    def _calculate_radial_positions(self, amount, distance):
        """Generate positons around (0,0) in a circle with radius = distance in pixels, angles between them is 2pi/amount of locations wanted"""
        angle = 2*math.pi / amount
        newDistance = distance
        radius = newDistance*math.pi*2
        print "%f, %f, %f" %(radius, radius/amount, self.size/4)
        while newDistance*math.pi*2/amount <= self.size*1.5:
            print "********************\newdistance = %s\n**************" % newDistance
            newDistance += (distance/4)
        #Start moving the parent so that the children won't overlap the siblings of the parent
        grandParent = self.parent.parentGroup.parent
        if grandParent:
            dist = self._dist_from_closest_sibling()
            distNeeded = newDistance * 1.2
            while dist <= distNeeded:
                print "%s, %s" % (dist, distNeeded)
                self.parent.move_offset(utils.a_to_b(grandParent.rect.center, self.parent.rect.center, 0.75))
                dist = self._dist_from_closest_sibling()
        #Create a list of tuples with coordinates representing radial offsets
        thelist =  [( math.sin(x*angle) * newDistance, math.cos(x*angle) * newDistance ) for x in range(amount)]
        return thelist

    
    def draw(self, destination):
        pygame.sprite.Group.draw(self, destination)
        for sprite in self.sprites():
            source = sprite.pathImage
            sourceRect = sprite.rect
            pos = (sourceRect.topright[0], sourceRect.topright[1] - (source.get_rect().h/2))
            destination.blit(source, pos)
            
    def update_zoom(self, zoom):
        self.zoom = zoom
        self._gen_image()
        for sprite in self.sprites():
            sprite.update_image()
    
    def _dist_from_closest_sibling(self):
        grandParent = self.parent.parentGroup.parent
        siblingList = [aunt.rect.center for aunt in grandParent.children]
        return min( filter(lambda x: x!= 0.0, map(lambda x: utils.distance(x, self.parent.rect.center), siblingList) )) if len(siblingList) > 1 else 50000